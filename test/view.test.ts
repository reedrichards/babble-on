import { rootView, renderIfAuthenticated, constructDate } from "../lib/views"

describe("rootView", () => {
    it("renders frame when user exists and authed", () => {
        const mockReq = {
            user: jest.fn(() => true),
            isAuthenticated: jest.fn(() => true),
        };
        const mockRes = {
            render: jest.fn()
        };
        rootView(mockReq, mockRes);
        // should call this
        expect(mockRes.render.mock.instances.length).toBe(1);
        expect(mockRes.render.mock.calls[0][0]).toBe("pages/frame");
    })
    it("renders login when there is no request user", () => {
        const mockReq = {
            user: jest.fn(() => false),
            isAuthenticated: jest.fn(() => false),
        };
        const mockRes = {
            render: jest.fn(),
        };
        rootView(mockReq, mockRes);
        // should call this
        expect(mockRes.render.mock.instances.length).toBe(1);
        expect(mockRes.render.mock.calls[0][0]).toBe("pages/login");
    })
})

describe("renderIfAuthenticated", () => {
    it("renders page when authed", () => {
        const mockReq = {
            user: jest.fn(() => true),
            isAuthenticated: jest.fn(() => true),
        };
        const mockRes = {
            render: jest.fn(),
            redirect: jest.fn()
        };
        renderIfAuthenticated(mockReq, mockRes, "pagename");
        // should call this
        expect(mockRes.render.mock.instances.length).toBe(1);
        expect(mockRes.render.mock.calls[0][0]).toBe("pagename");
    })
    it("renders login when there is no request user", () => {
        const mockReq = {
            user: jest.fn(() => false),
            isAuthenticated: jest.fn(() => false),
        };
        const mockRes = {
            render: jest.fn(),
            redirect: jest.fn()
        };
        renderIfAuthenticated(mockReq, mockRes, "pagename");
        // should call this
        expect(mockRes.redirect.mock.instances.length).toBe(1);
        expect(mockRes.redirect.mock.calls[0][0]).toBe("/");
    })
})

describe("constructDate ", () => {
    it("works only year", () => {
        let date = constructDate(1);
        expect(date.year).toBe(1);
        expect(date.month).toBeUndefined();
        expect(date.day).toBeUndefined();
    })
    it("works all values", () => {
        let date = constructDate(1, 2, 3);
        expect(date.year).toBe(1);
        expect(date.month).toBe(2);
        expect(date.day).toBe(3);
    })
    it("works without year", () => {
        let date = constructDate(undefined, 2, 3);
        expect(date.year).toBeUndefined();
        expect(date.month).toBe(2);
        expect(date.day).toBe(3);
    })
})