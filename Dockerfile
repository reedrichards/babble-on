FROM node
WORKDIR /src/
RUN mkdir -p /mnt/data
COPY package.json .
COPY package-lock.json .
RUN npm install
RUN npm install -g typescript
COPY . .
CMD npm run start
