* Babble On
  :PROPERTIES:
  :CUSTOM_ID: google-photos-library-api-samples
  :END:

This is a Node.js sample application for the
[[https://developers.google.com/photos][Google Photos Library API]].

This sample shows how to connect an app with Google Photos through OAuth
2.0 and display a user's photos and albums in an "online photo frame".

This app is built using [[https://expressjs.com/][Express.js]] and
[[https://getmdl.io/][Material Design Lite]].

** getting started
*** [[https://console.cloud.google.com/apis/credentials?project=babelon-1574732862161][get oauth credentials]]
*** create .env file with your credentials
#+BEGIN_SRC txt
ClientId=4206942069-dfklsdjflkdsjflksdjvosidvj03vroenv.apps.googleusercontent.com
ClientSecret=JOIMsa_KJOIMcssomckd
#+END_SRC
*** downloady keys from service account for buckets and save to ~keys.json~
*** run the service
#+BEGIN_SRC bash
docker-compose up -d
#+END_SRC
** App design

  The app is built using the [[https://expressjs.com/][Express.js]]
  framework and the [[http://ejs.co/][ejs]] templating system.

  First, the user has to log in via OAuth 2.0 and authorize the
  =https://www.googleapis.com/auth/photoslibrary.readonly= scope. (See the
  file =config.js=.) Once authenticated, photos are loaded into the photo
  frame via search or from an album.

  The app is split into the backend (=app.js=) and the front end
  (=static/...=). The photo frame preview screen make AJAX requests to the
  backend to load a list of selected photos. Likewise, the album screen
  makes an AJAX request to the backend to load the list of albums that are
  owned by the user. The backend returns media items or albums directly
  from the Library API that are parsed and rendered in the browser.

*** Pages

**** ~/~ Login page

     by default shows login screen, otherwise redirect to ~/albums~

**** ~/albums~ lists albums available for trancsription

     lists all albums on page, with ability to navigate to album in google
     photos. Also user's can select albums to be transcribed on this page

**** ~/cart~ lists all albums to be transcribed

     lists all of the albums that are going to be transcribed, warns user when
     overwriting data

**** ~/transcriptions~ list all transcripted albums, with options to preview and download

     users can access there transcripted albums and download / redownload their
     transcriptions, and edit the results.

**** ~/search~ create transcriptions from search terms

*** Project Structure
    https://emacs.stackexchange.com/questions/38135/wrap-cell-content-in-an-org-mode-table
    #+BEGIN_SRC text
    +------------+-----------------------------------------------------------+
    | Name       | Description                                               |
    +------------+-----------------------------------------------------------+
    | lib        | main folder for app development, entrypoint for backend is|
    |            | ~app.js~,  frontentd code lives in ~lib/static~           |
    +------------+-----------------------------------------------------------+
    | lib/static | main folder for frontend javascript                       |
    +------------+-----------------------------------------------------------+
    | images     | static images for project                                 |
    +------------+-----------------------------------------------------------+
    | built      | location for compiled assets                              |
    +------------+-----------------------------------------------------------+
    | views      | [[http://ejs.co/][ejs]] templates location                |
    +------------+-----------------------------------------------------------+
    | static     | static non js assets go here, ~css~                       |
    +------------+-----------------------------------------------------------+
    #+END_SRC
*** API Use and code overview
    :PROPERTIES:
    :CUSTOM_ID: api-use-and-code-overview
    :END:


**** Search
     :PROPERTIES:
     :CUSTOM_ID: search
     :END:

  The search screen (=/search=) is loaded from a template file located at
  =views/pages/search.ejs= that contains the search form. When this form
  is submitted, the POST request is received in =app.js= in the handler
  =app.post('/loadFromSearch', ...)=. The values sent in the form are used
  to prepare a [filter] object that is then submitted to the Google Photos
  Library API =/search= endpoint in
  =libraryApiSearch(authToken, parameters)=.

  The call to the API in =libraryApiSearch(authToken, parameters)= shows
  how to handle the =nextPageToken= to retrieve multiple pages of search
  results.

**** Albums
     :PROPERTIES:
     :CUSTOM_ID: albums
     :END:

  The album screen (=/album=) is loaded from a template file located at
  =views/pages/album.ejs=. When this screen is loaded, the browser makes a
  request to =/getAlbums= that is received by the server =app.js= in the
  handler =app.get('/getAlbums', ...)=. The method
  =libraryApiGetAlbums(authToken)= is called to load the albums from the
  API. This method shows to handle the =nextPageToken= to retrieve a
  complete list of all albums owned by the user.

  The retrieved [=albums=] are returned and displayed through the file
  =static/js/album.js=. Here the =album= objects are parsed and the title,
  cover photo and number of items are rendered on screen.

  When an album is selected, the handler =app.post('/loadFromAlbum', ...)=
  receives the id of the album that was picked. Here, a search parameter
  is constructed and passed to =libraryApiSearch(authToken, parameters)=
  to load the images.

**** Displaying photos
     :PROPERTIES:
     :CUSTOM_ID: displaying-photos
     :END:

  Photos are displayed on the photo frame page of this app. The template
  file is located at =views/pages/frame.ejs=. When this page is loaded, a
  request is made to =app.get('/getQueue', ...)= to the server =app.js=.

  This handler returns a list of the =mediaItems= the user has loaded into
  the frame through search or from an album. They are rendered for display
  by the browser through the file =static/js/frame.js=. Here the caption
  is constructed from the description and other media metadata. A
  thumbnail, scaled based on the original height and width, is used to
  render a preview initially while the image at full resolution is being
  loaded.

*** Troubleshooting
    :PROPERTIES:
    :CUSTOM_ID: troubleshooting
    :END:

  Make sure that you have configured the =Client ID= and the
  =Client secret= in the configuration file =config.js=. Also check that
  the URLs configured for these credentials match how you access the
  server. By default this is configured for 127.0.0.1 (localhost) on port
  8080.

  You can also start the app with additional debug logging by setting the
  =DEBUG= environment variable to =true=. For example:

  #+BEGIN_EXAMPLE
    DEBUG=TRUE node app.js
  #+END_EXAMPLE
** Testing
   :PROPERTIES:
   :CUSTOM_ID: testing
   :END:

For this project, I chose [[https://facebook.github.io/jest/][Jest]] as
our test framework. While Mocha is probably more common, Mocha seems to
be looking for a new maintainer and setting up TypeScript testing in
Jest is wicked simple.

*** Install the components
    :PROPERTIES:
    :CUSTOM_ID: install-the-components
    :END:

To add TypeScript + Jest support, first install a few npm packages:

#+BEGIN_EXAMPLE
    npm install -D jest ts-jest
#+END_EXAMPLE

=jest= is the testing framework itself, and =ts-jest= is just a simple
function to make running TypeScript tests a little easier.

*** Configure Jest
    :PROPERTIES:
    :CUSTOM_ID: configure-jest
    :END:

Jest's configuration lives in =jest.config.js=, so let's open it up and
add the following code:

#+BEGIN_SRC js
    module.exports = {
        globals: {
            'ts-jest': {
                tsConfigFile: 'tsconfig.json'
            }
        },
        moduleFileExtensions: [
            'ts',
            'js'
        ],
        transform: {
            '^.+\\.(ts|tsx)$': './node_modules/ts-jest/preprocessor.js'
        },
        testMatch: [
            '**/test/**/*.test.(ts|js)'
        ],
        testEnvironment: 'node'
    };
#+END_SRC

Basically we are telling Jest that we want it to consume all files that
match the pattern ="**/test/**/*.test.(ts|js)"= (all
=.test.ts=/=.test.js= files in the =test= folder), but we want to
preprocess the =.ts= files first. This preprocess step is very flexible,
but in our case, we just want to compile our TypeScript to JavaScript
using our =tsconfig.json=. This all happens in memory when you run the
tests, so there are no output =.js= test files for you to manage.

*** Running tests
    :PROPERTIES:
    :CUSTOM_ID: running-tests
    :END:

Simply run =npm run test=. Note this will also generate a coverage
report.

*** Writing tests
    :PROPERTIES:
    :CUSTOM_ID: writing-tests
    :END:

Writing tests for web apps has entire books dedicated to it and best
practices are strongly influenced by personal style, so I'm deliberately
avoiding discussing how or when to write tests in this guide. However,
if prescriptive guidance on testing is something that you're interested
in, [[https://www.surveymonkey.com/r/LN2CV82][let me know]], I'll do
some homework and get back to you.
