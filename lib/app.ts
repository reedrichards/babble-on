// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

"use strict";

const path = require('path');
const bodyParser = require("body-parser");
const express = require("express");
const expressWinston = require("express-winston");
const http = require("http");
const winston = require("winston");
const { Storage } = require("@google-cloud/storage");
var graphqlHTTP = require("express-graphql");
var { buildSchema } = require("graphql");
import {
    rootView, translateView, logoutView, searchView, albumView,
    loadFromSearchView, loadFromAlbumView, getAlbumsView, getQueueView
} from "./views";
const { localUserSessionMiddleware, sessionMiddleware } = require('./middlewares');
import config from "./config"


// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
const stripe = require('stripe')('sk_test_4eC39HqLyjWDarjtT1zdp7dc');

// Begin app
const app = express();
const server = http.Server(app);
// storage for images bucket
const googleStorage = new Storage({ keyFilename: "/src/keys.json" });
const zipBucket = require("zip-bucket")(googleStorage);
// Use the EJS template engine
app.set("view engine", "ejs");

// Set up OAuth 2.0 authentication through the passport.js library.
const passport = require("passport");
import auth from "./auth"
auth(passport);


// Console transport for winton.
const consoleTransport = new winston.transports.Console();

// Set up winston logging.
const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple()
  ),
  transports: [consoleTransport]
});

// Enable extensive logging if the DEBUG environment variable is set.
if (process.env.DEBUG) {
  // Print all winston log levels.
  logger.level = "silly";

  // Enable express.js debugging. This logs all received requests.
  app.use(
    expressWinston.logger({
      transports: [consoleTransport],
      winstonInstance: logger
    })
  );
  // Enable request debugging.
  require("request-promise").debug = true;
} else {
  // By default, only print all 'verbose' log level messages or below.
  logger.level = "verbose";
}

// Begin static routes
// Set up static routes for hosted libraries.
app.use(express.static(path.join(__dirname, "../static/")));
app.use("/js", express.static(__dirname + "/static/js"));
app.use("/js", express.static(path.join(__dirname, "/../node_modules/jquery/dist/")));

app.use(
  "/fancybox",
    express.static(path.join(__dirname, "/../node_modules/@fancyapps/fancybox/dist/")));

app.use(
  "/mdlite",
    express.static(path.join(__dirname, "/../node_modules/material-design-lite/dist/")));
// End static routes

// Parse application/json request data.
app.use(bodyParser.json());

// Parse application/xwww-form-urlencoded request data.
app.use(bodyParser.urlencoded({ extended: true }));


// Begin middlewares and session handling
// these are together because they are tightly coupled

// Enable user session handling.
app.use(sessionMiddleware);

// Set up passport and session handling.
app.use(passport.initialize());
app.use(passport.session());


app.use(localUserSessionMiddleware);

// End middleware and session handling

app.get("/", rootView);

app.get("/translate", translateView);

app.get("/logout", logoutView);

// Star the OAuth login process for Google.
app.get( "/auth/google",
  passport.authenticate("google", {
    scope: config.scopes,
    failureFlash: true, // Display errors to the user.
    session: true
  })
);

// Callback receiver for the OAuth process after log in.
app.get( "/auth/google/callback",
  passport.authenticate("google", {
    failureRedirect: "/",
    failureFlash: true,
    session: true
  }),
  (req, res) => {
    // User has logged in.
    logger.info("User has logged in.");
    res.redirect("/");
  }
);

app.get("/search", searchView);

app.get("/album", albumView);

app.post("/loadFromSearch", loadFromSearchView);

app.post("/loadFromAlbum", loadFromAlbumView);

app.get("/getAlbums", getAlbumsView);

app.get("/getQueue", getQueueView);

// Start the server
server.listen(config.port, () => {
  console.log(`App listening on port ${config.port}`);
  console.log("Press Ctrl+C to quit.");
});

// begin graphql
// Graphql schema definition for babelon backend

// Construct a schema, using GraphQL schema language
const schema = buildSchema(`
  type StripeSession {
    id: ID!
  }

  type Query {
    allTranslations(albumId: String!): String
    stripeSessionQuery: StripeSession
  }
`);

class StripeSession {
  id: string;
  constructor(session) {
    this.id = session.id;
  }
}

// The root provides a resolver function for each API endpoint
const root = async (request, response, graphQLParams) => ({
  stripeSessionQuery: async ({}, request) => {
    const session = await stripe.checkout.sessions.create({
      payment_method_types: ['card'],
      line_items: [{
        name: 'Transcription',
        description: 'Comfortable cotton t-shirt',
        images: ['https://example.com/t-shirt.png'],
        amount: 500,
        currency: 'usd',
        quantity: 1,
      },
      {
        name: 'Transcription',
        description: 'Comfortable cotton t-shirt',
        images: ['https://i.imgur.com/hc522lW.jpg'],
        amount: 500,
        currency: 'usd',
        quantity: 3,
      }],
      success_url: 'http://127.0.0.1:8080/success?session_id={CHECKOUT_SESSION_ID}',
      cancel_url: 'http://127.0.0.1:8080/translate',
    });
      return new StripeSession(session);
  },
  // Returns link to zipped translations
  allTranslations: async ({ albumId }, request, response) => {
    if (!request.user || !request.isAuthenticated()) {
      // Not logged in yet.
      return response.render("pages/login");
    } else {
      // build file path from request to use when looking up storage bucket
      const filepath = `${request.user.profile.id}/${albumId}/`;
      const zipPath = filepath + albumId + ".zip";
      zipBucket({
        fromBucket: config.translationsBucket,
        fromPath: filepath,
        toBucket: config.translationsBucket,
        toPath: zipPath
      });
      console.log("created: ", zipPath, " from ", filepath);
      // These options will allow temporary read access to the file
      const options = {
        version: "v2", // defaults to 'v2' if missing.
        action: "read",
        expires: Date.now() + 1000 * 60 * 60 // one hour
      };
      const [url] = await googleStorage
        .bucket(config.translationsBucket)
        .file(zipPath)
        .getSignedUrl(options);

      console.log("generated signed url: ", url);
      return url;
    }
  }
});

app.use(
  "/graphql",
  graphqlHTTP(async (request, response, graphQLParams) => ({
    schema: schema,
    rootValue: await root(request, response, graphQLParams),
    graphiql: true
  }))
);

// end graphql

// [END app]
