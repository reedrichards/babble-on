const session = require("express-session");
const sessionFileStore = require("session-file-store");

// Middleware that adds the user of this session as a local variable,
// so it can be displayed on all pages when logged in.
exports.localUserSessionMiddleware = (req, res, next) => {
  res.locals.name = "-";
  if (req.user && req.user.profile && req.user.profile.name) {
    res.locals.name =
      req.user.profile.name.givenName || req.user.profile.displayName;
  }

  res.locals.avatarUrl = "";
  if (req.user && req.user.profile && req.user.profile.photos) {
    res.locals.avatarUrl = req.user.profile.photos[0].value;
  }
  next();
};


const fileStore = sessionFileStore(session);

// Set up a session middleware to handle user sessions.
//
// TODO: A secret is used to sign the cookie. This is just used for this sample
// app and should be changed.
exports.sessionMiddleware = session({
  resave: true,
  saveUninitialized: true,
  store: new fileStore({}),
  secret: "photo frame sample"
});
