
var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');

const getStripeSession = () => {
  var query = `
  query StripeSessionId{
    stripeSessionQuery {
      id
    }
  }
  `;
  return fetch("/graphql", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify({ query })
  })
    .then(r => r.json())
    .then(data => {
      return data
    });
}

const downloadAllTranslations = albumId => {
  var query = `
query AllTranslationsLink($albumId: String!){
    allTranslations(albumId: $albumId)
  }
  `;
  fetch("/graphql", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify({ query, variables: { albumId: albumId } })
  })
    .then(r => r.json())
    .then(data => {
      console.log("data returned:", data.data.allTranslations);
      window.location = data.data.allTranslations;
    });
};

// Makes a backend request to display the queue of photos currently loaded into
// the photo frame. The backend returns a list of media items that the user has
// selected. They are rendered in showPreview(..).
function createDownloadLink() {
  showLoadingDialog();
  $.ajax({
    type: "GET",
    url: "/getQueue",
    dataType: "json",
    success: data => {
      // Queue has been loaded. Display the media items as a grid on screen.
      hideLoadingDialog();
      var getAllTranslationsButton = document.getElementById(
        "getAllTranslationsButton"
      );
      getAllTranslationsButton.onclick = () => getStripeSession().then((data) =>
        stripe.redirectToCheckout({
          // Make the id field from the Checkout Session creation API response
          // available to this file, so you can provide it as parameter here
          // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
          sessionId: data.data.stripeSessionQuery.id
        }).then(function (result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
        }))
      // downloadAllTranslations(data.parameters.albumId)
      hideLoadingDialog();
      console.log("Created download link.");
    },
    error: data => {
      hideLoadingDialog();
      handleError("Could not load queue", data);
    }
  });
}

$(document).ready(() => {

  createDownloadLink()
})
